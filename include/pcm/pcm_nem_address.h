//
//  pcm_nem_address.h
//  pcm
//
//  Created by Alex Melnichuk on 5/13/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_NEM_ADRESS_H
#define PCM_NEM_ADRESS_H

#include <math.h>
#include <stdio.h>
#include <curve25519/ed25519/ge.h>
#include "pcm_crypto.h"

#define PCM_NEM_PRIVKEY_PART_BYTE_COUNT 32
#define PCM_NEM_PRIVKEY_HEX_CHAR_COUNT 64
#define PCM_NEM_PRIVKEY_BYTE_COUNT 64
#define PCM_NEM_PUBKEY_BYTE_COUNT 32
#define PCM_NEM_ADDRESS_DECODED_BYTE_COUNT 25
#define PCM_NEM_ADDRESS_ENCODED_CHAR_COUNT 40
#define PCM_NEM_ADDRESS_ENCODED_NORMALIZED_CHAR_COUNT 46
#define PCM_NEM_ADDRESS_CHECKSUM_BYTE_COUNT 4

bool pcm_nem_address_valid(const char* in_address, uint8_t in_scheme);
void pcm_nem_address_normalize(const char* in_address, size_t in_address_length, char* out_normalized_address);
void pcm_nem_address_denormalize(const char* in_address,
                                 size_t in_address_length,
                                 size_t* out_normalized_address_length,
                                 char* out_normalized_address);
pcm_result pcm_nem_address(const void* in_pubkey, uint8_t in_scheme, int8_t* out_address_bytes);
pcm_result pcm_nem_public_key(const void* in_privkey, uint8_t* out_pubkey);
pcm_result pcm_nem_private_key(const char* in_seed, uint8_t* out_privkey);

#endif
