//
//  pcm_util.h
//  pcm
//
//  Created by Alex Melnichuk on 3/9/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_UTIL_H
#define PCM_UTIL_H

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

uint16_t pcm_swap_uint16(uint16_t val);

int16_t pcm_swap_int16(int16_t val);

uint32_t pcm_swap_uint32(uint32_t val);

int32_t pcm_swap_int32(int32_t val);

int64_t pcm_swap_int64(int64_t val);

uint64_t pcm_swap_uint64(uint64_t val);

int64_t pcm_current_time_in_millis(void);

void pcm_print_hex(const void* in_bytes, size_t in_length);

bool pcm_is_little_endian(void);

void pcm_to_hex(const uint8_t* in_string, size_t in_length, char* out_hex_string);

bool pcm_from_hex(const char* in_hex_string, size_t in_length, uint8_t* out_bytes);
void pcm_reverse(void* in_bytes, size_t in_length);
void pcm_reverse_uint64(void* in_bytes, size_t in_length);

#endif
