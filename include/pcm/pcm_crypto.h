//
//  pcm_crypto.h
//  pcm
//
//  Created by Alex Melnichuk on 3/17/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_CRYPTO_H
#define PCM_CRYPTO_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <keccak-tiny/keccak-tiny.h>
#include <keccak-tiny/sha3.h>
#include <base32/base32.h>
#include <openssl/sha.h>
#include <openssl/rand.h>
#include <openssl/ripemd.h>
#include <secp256k1/secp256k1.h>
#include <curve25519/curve25519.h>

#include <cyclone_crypto/cyclone_crypto.h>
#include <base58/base58.h>

#include "pcm_result.h"
#include "pcm_util.h"

#define PCM_BLAKE2B256_BYTE_COUNT 32 // 256 bits
#define PCM_KECCAK256_BYTE_COUNT 32
#define PCM_KECCAK512_BYTE_COUNT 64
#define PCM_SHA256_BYTE_COUNT 32
#define PCM_RIPEMD160_BYTE_COUNT 20
#define PCM_ECDSA_SERIALIZED_PUBKEY_BYTE_COUNT 65
#define pcm_btc_hash pcm_sha256_sha256
#define pcm_hash_160 pcm_ripemd160_sha256

pcm_result pcm_sha256_sha256(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_ripemd160_sha256(const void* in_data, size_t in_length, uint8_t* out_bytes);

pcm_result pcm_random_bytes(uint8_t* out_buffer, size_t in_length);
pcm_result pcm_blake2b(const void* in_data, size_t in_length, uint8_t* out_bytes, size_t out_length);
pcm_result pcm_blake2b256(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_keccak256(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_keccak512(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_sha512(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_sha256(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_ripemd160(const void* in_data, size_t in_length, uint8_t* out_bytes);
pcm_result pcm_ecdsa_public_key(const void* in_privkey, uint8_t* out_bytes);
pcm_result pcm_curve25519_donna(const void* in_privkey, uint8_t* out_bytes);

size_t pcm_base32_encoded_length(size_t in_length);
size_t pcm_base32_decoded_length(size_t in_length);
pcm_result pcm_base32_encode(const void* in_data, size_t in_length, int8_t *out_bytes);
pcm_result pcm_base32_decode(const void* in_data, size_t in_length, uint8_t* out_bytes);

pcm_result pcm_base58_encode(const void* in_bytes,
                             size_t in_length,
                             uint8_t* out_base58,
                             size_t* out_length);

pcm_result pcm_base58_decode_init(const void* in_base58,
                             size_t in_length,
                             uint8_t** out_bytes,
                             size_t* out_length);

void pcm_base58_decode_destroy(uint8_t** in_decoded_array);

pcm_result pcm_base58_check(const char* in_str, uint8_t in_pubkeyhash);

pcm_result pcm_base58_check_decode(const char* in_str,
                                   uint8_t in_version,
                                   uint8_t** out_bytes,
                                   size_t* out_length);

pcm_result pcm_base58_check_encode(const void* in_bytes,
                                   size_t in_length,
                                   uint8_t in_version,
                                   char* out_base58,
                                   size_t* out_length);

bool pcm_sha256_libbase58_bridge(void *out_digest, const void *in_data, size_t in_length);

#endif 
