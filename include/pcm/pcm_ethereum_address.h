//
//  pcm_ethereum_address.h
//  pcm
//
//  Created by Alex Melnichuk on 3/17/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_ETHEREUM_ADDRESS_H
#define PCM_ETHEREUM_ADDRESS_H

#include "pcm_crypto.h"

#define PCM_ETHEREUM_ECDSA_SERIALIZED_PUBKEY_BYTE_COUNT 64
#define PCM_ETHEREUM_ADDRESS_BYTE_COUNT 20
#define PCM_ETHEREUM_ADDRESS_KECCAK256_OFFSET 12 // last 20 bytes are taken, 32 - 20 = 12
#define PCM_ETHEREUM_ADDRESS_CHARACTER_COUNT 42  // warning: '\0' not included
#define PCM_ETHEREUM_ADDRESS_CHARACTER_COUNT_NO_SUFFIX 40

pcm_result pcm_ethereum_address(const void* in_pubkey, char* out_address);
pcm_result pcm_ethereum_public_key(const void* in_privkey, uint8_t* out_pubkey);

#endif 
