//
//  pcm_tezos.h
//  pcm
//
//  Created by Alex Melnichuk on 4/10/19.
//  Copyright © 2019 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_TEZOS_H
#define PCM_TEZOS_H

#include <curve25519/ed25519/ge.h>
#include <curve25519/ed25519/sc.h>
#include <curve25519/ed25519/additions/keygen.h>
#include <curve25519/ed25519/additions/crypto_hash_sha512.h>
#include "pcm_crypto.h"

#define PCM_TEZOS_PRIVKEY_BYTE_COUNT 32
#define PCM_TEZOS_PUBKEY_BYTE_COUNT 32
#define PCM_TEZOS_ADDRESS_BYTE_COUNT 20
#define PCM_TEZOS_SIGNATURE_BYTE_COUNT 64
#define PCM_TEZOS_SIGNATURE_HASH_BYTE_COUNT 32

pcm_result pcm_tezos_public_key(const void* in_privkey, uint8_t* out_pubkey);
pcm_result pcm_tezos_address(const void* in_pubkey, uint8_t* out_address);
pcm_result pcm_tezos_sign(const void* in_message, size_t in_message_length, const void* in_privkey, uint8_t* out_signature);
pcm_result pcm_tezos_sign_hash(const void* in_data, size_t in_length, uint8_t* out_signature_hash);

#endif /* PCM_TEZOS_H */
