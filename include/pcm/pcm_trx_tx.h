//
//  pcm_trx_tx.h
//  pcm
//
//  Created by Alex Melnichuk on 12/26/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_TRX_TX_H
#define PCM_TRX_TX_H

#define PCM_TRX_SIGNATURE_BYTE_COUNT 65

#include <stdio.h>
#include <secp256k1/secp256k1.h>
#include <secp256k1/secp256k1_recovery.h>
#include <openssl/bn.h>
#include <openssl/ecdsa.h>
#include <openssl/obj_mac.h>
#include "pcm_crypto.h"

pcm_result pcm_trx_sign_transaction(const uint8_t* in_data, size_t in_length,
                                    const uint8_t* in_privkey, size_t in_privkey_length,
                                    uint8_t* out_signature);

#endif 
