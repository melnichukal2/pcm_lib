//
//  pcm_nem_tx.h
//  pcm
//
//  Created by Alex Melnichuk on 5/17/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_NEM_TX_H
#define PCM_NEM_TX_H

#define PCM_NEM_SIGNATURE_PART_BYTE_COUNT 32
#define PCM_NEM_SIGNATURE_BYTE_COUNT 64

#include <curve25519/ed25519/sc.h>
#include "pcm_nem_address.h"

pcm_result pcm_nem_sign_transaction(const void* in_data,
                                    size_t in_length,
                                    const uint8_t* in_pubkey,
                                    const uint8_t* in_privkey,
                                    uint8_t* out_signature);

#endif
