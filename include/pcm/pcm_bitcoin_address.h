//
//  pcm_bitcoin_address.h
//  pcm
//
//  Created by Alex Melnichuk on 6/2/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_BITCOIN_ADDRESS_H
#define PCM_BITCOIN_ADDRESS_H

#include <stdio.h>
#include "pcm_crypto.h"



typedef struct pcm_hash160_context {
    uint8_t* hash;
    size_t hash_length;
} pcm_hash160_context;

pcm_result pcm_hash_160_init_from_address(pcm_hash160_context* c, const char* in_address, size_t in_address_length);
pcm_result pcm_hash_160_init_from_pubkey(pcm_hash160_context* c, const void* in_pubkey, size_t in_length);
pcm_result pcm_hash_160_destroy(pcm_hash160_context* c);

#endif
