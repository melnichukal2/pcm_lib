//
//  pcm_eos_tx.h
//  pcm
//
//  Created by Alex Melnichuk on 6/19/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_EOS_TX_H
#define PCM_EOS_TX_H

#define PCM_EOS_SIGNATURE_BYTE_COUNT 65

#include <stdio.h>
#include <secp256k1/secp256k1.h>
#include <secp256k1/secp256k1_recovery.h>

#include <openssl/bn.h>
#include <openssl/ecdsa.h>
#include <openssl/obj_mac.h>

#include "pcm_crypto.h"

bool pcm_eos_validate_symbol(const char* in_str);

uint64_t pcm_eos_encode_name(const char* in_str);

int pcm_secp256k1_nonce_function(unsigned char *nonce32,
                                 const unsigned char *msg32,
                                 const unsigned char *key32,
                                 const unsigned char *algo16,
                                 void *data,
                                 unsigned int counter);

pcm_result pcm_eos_sign_transaction(const uint8_t* in_data,
                                    const uint8_t* in_privkey,
                                    size_t in_privkey_length,
                                    const uint8_t* in_pubkey,
                                    size_t in_pubkey_length,
                                    uint8_t* out_signature);


#endif
