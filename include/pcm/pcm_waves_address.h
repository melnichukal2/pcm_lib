//
//  pcm_waves_address.h
//  pcm
//
//  Created by Alex Melnichuk on 3/17/18.
//  Copyright © 2018 Alex Melnichuk. All rights reserved.
//

#ifndef PCM_WAVES_ADDRESS_H
#define PCM_WAVES_ADDRESS_H

#include "pcm_crypto.h"

#define PCM_WAVES_BASEPOINT_BYTE_COUNT 32
#define PCM_WAVES_SECURE_HASH_BYTE_COUNT PCM_KECCAK256_BYTE_COUNT
#define PCM_WAVES_PRIVKEY_BYTE_COUNT PCM_SHA256_BYTE_COUNT
#define PCM_WAVES_PUBKEY_BYTE_COUNT 32
#define PCM_WAVES_ADDRESS_BYTE_COUNT 26
#define PCM_WAVES_ADDRESS_CHECKSUM_BYTE_COUNT 4

pcm_result pcm_waves_address_valid(const char* in_address, uint8_t in_scheme);
pcm_result pcm_waves_address(const void* in_pubkey, uint8_t in_scheme, uint8_t* out_address_bytes);
void pcm_waves_public_key(const void* in_privkey, uint8_t* out_pubkey);
pcm_result pcm_waves_private_key(const void* in_seed, size_t in_seed_length, int32_t in_nonce, uint8_t* out_privkey);
pcm_result pcm_waves_secure_hash(const void* in_data, size_t in_length, uint8_t* out_bytes);

#endif 
