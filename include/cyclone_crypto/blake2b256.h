#ifndef _BLAKE2B256_H
#define _BLAKE2B256_H

#include "crypto.h"
#include "blake2b.h"

//BLAKE2b-256 block size
#define BLAKE2B256_BLOCK_SIZE 128
//BLAKE2b-256 digest size
#define BLAKE2B256_DIGEST_SIZE 32
//Common interface for hash algorithms
#define BLAKE2B256_HASH_ALGO (&blake2b256HashAlgo)

typedef Blake2bContext Blake2b256Context;


//BLAKE2b-256 related constants
extern const HashAlgo blake2b256HashAlgo;

//BLAKE2b-256 related functions
error_t blake2b256Compute(const void *data, size_t length, uint8_t *digest);
void blake2b256Init(Blake2b256Context *context);
void blake2b256Update(Blake2b256Context *context, const void *data, size_t length);
void blake2b256Final(Blake2b256Context *context, uint8_t *digest);

#endif
